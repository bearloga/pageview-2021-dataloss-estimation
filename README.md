# Pageview 2021 dataloss estimation

For [T304876](https://phabricator.wikimedia.org/T304876), to facilitate [T303998](https://phabricator.wikimedia.org/T303998)

From [Product Health Timelines (pageview info)](https://docs.google.com/presentation/d/1EFg6oa5WNbrLVHWkQqI5vHLs4HRNMtHvBxoF2_8QULA/edit#slide=id.g1151a16c020_0_12) \[Slides\]:
> In June of 2021, a caching node in one of the US data centers stopped collecting traffic data. In November of 2021, an additional 2 nodes in a second US data center were affected.
>
> We estimate this resulted in underreporting global pageviews by 2 to 4% between 4 June and 3 October, and by 5 to 8% between 4 November and 27 January.

Relevant links:
- [T239811](https://phabricator.wikimedia.org/T239811) – where bot traffic inflation of 2019 was identified
  - Mobile web pageviews in the US were impacted by bot traffic in 2019 and 2020. (Source: [T239811#7805575](https://phabricator.wikimedia.org/T239811#7805575))
- [T300164](https://phabricator.wikimedia.org/T300164) – details of data loss
  - varnishkafka-webrequest on cp1087, cp4035 and cp4036 (text)
  - The caching nodes affected were:
    - cp\[6002,6005,6009-6013\].drmrs.wmnet
        - Marseille, France (Europe)
        - Edge caching
    - cp1087.eqiad.wmnet
        - Ashburn, Virginia, United States (North America) 
        - Core services
    - cp\[4021,4033-4036\].ulsfo.wmnet
        - San Francisco, California, United States (North America)
        - Edge caching
  - [en6C + JP + DE Pageviews Corrected for data loss](https://docs.google.com/spreadsheets/d/1J45El5yOq5EP4v_ae7qbZDhnDq-dqQ1JmcPawWKWMRk/edit#) \[Spreadsheet\]
  - [Pageview Loss with Decline](https://docs.google.com/spreadsheets/d/14iCinouU4mDjI_JyXa64RDPKl9T3GVzBGOFgaUU3G78/edit#) \[Spreadsheet\]
- [wikitech:Data centers](https://wikitech.wikimedia.org/wiki/Data_centers)
  - [operations/dns/geo-maps](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/dns/+/refs/heads/master/geo-maps)

"en6c": 6 English-as-primary-language countries)
- US (United States)
- CA (Canada)
- GB (UK)
- IE (Ireland)
- AU (Australia)
- NZ (New Zealand)

Also interested in JP (Japan) and DE (Germany)

From [T300164#7690173](https://phabricator.wikimedia.org/T300164#7690173):
> Between 2021-06-04 and 2021-11-03 we have lost 2.80% of webrequest-text, statv and eventlogging data (pageview impact, eqiad datacenter only)
>
> Between 2021-11-04 and 2022-01-27 we have lost 4.34% of webrequest-text, statv and eventlogging data (pageview impact, eqiad + ulsfo datacenters only)

From [June 2021 Datacenter Switchover](https://techblog.wikimedia.org/2021/07/23/june-2021-data-center-switchover/):
> In June 2021, the Wikimedia Foundation’s Site Reliability Engineering team switched most user traffic from our primary datacenter in Virginia (“eqiad”) to our secondary one in Texas (“codfw”).
